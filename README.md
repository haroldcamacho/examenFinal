# **Procedimiento de ejecución**

### 1.Cree una base de datos con el nombre "portalPedido"

### 2.Compile el programa y ejecute el puerto localhost:8050

### 3.Se tiene los siguientes datos registrados:

* **ADMINISTRADOR**
* Email: diego@gmail.com
* Contraseña: 123
* **CLIENTES** cada cliente se encuentra registrado junto a dos direcciones.
* Email: pepito@gmail.com
* Contraseña: 123
* Email: juanita@gmail.com
* Contraseña: 123
* **PROPIETARIOS**
* Email: pablo@gmail.com(PROPIETARIO DEL RESTAURANTE "ELLIS")
* Contraseña: 123
* Email: anita@gmail.com(PROPIETARIA DEL RESTAURANTE "MALCRIADAS")
* Contraseña: 123
* **RESTAURANTES** cada restaurante se encuetra registrado junto a tres productos.
* Nombre:Ellis(propietario "pablo@gmail.com")
* Nombre:Malcriadas(propietaria "anita@gmail.com")
    
