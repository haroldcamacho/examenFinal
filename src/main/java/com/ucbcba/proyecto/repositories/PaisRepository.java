package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Pais;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface PaisRepository extends CrudRepository<Pais,Integer>{
}
