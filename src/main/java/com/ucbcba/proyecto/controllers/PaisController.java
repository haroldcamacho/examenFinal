package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Pais;
import com.ucbcba.proyecto.services.CityService;
import com.ucbcba.proyecto.services.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class PaisController {

    private PaisService paisService;
    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService){this.cityService=cityService;}

    @Autowired
    public void setPaisService(PaisService paisService){this.paisService=paisService;}

    @RequestMapping(value = "/admin/pais/new",method = RequestMethod.GET)
    public String newPais(Model model){

        model.addAttribute("pais",new Pais());
       // model.addAttribute("cities",cityService.listAllCitys());
        return "paisForm";
    }

   @RequestMapping(value = "/admin/pais",method = RequestMethod.POST)
   public String save(@Valid Pais pais, BindingResult bindingResult, Model model){
       if(bindingResult.hasErrors()){
           return "paisForm";
       }

       paisService.savePais(pais);

       return "redirect:/admin/principalA";
    }

       /* @RequestMapping(value = "/admin/pais",method = RequestMethod.POST)
    public String save(@Valid Pais pais, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            model.addAttribute("cities",cityService.listAllCitys());
            return "paisForm";
        }

        paisService.savePais(pais);

        return "redirect:/admin/principalA";
    }*/

}
