package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Pais;
import com.ucbcba.proyecto.repositories.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PaisServiceImpl implements PaisService {
    private PaisRepository paisRepository;

    @Autowired
    @Qualifier(value = "paisRepository")
    public void setPaisRepository(PaisRepository paisRepository) {
        this.paisRepository = paisRepository;
    }
    @Override
    public Iterable<Pais> listAllPaises() {
        return paisRepository.findAll();
    }

    @Override
    public Pais getPaisById(Integer id) {
        return paisRepository.findOne(id);
    }

    @Override
    public Pais savePais(Pais pais) {
        return paisRepository.save(pais);
    }

    @Override
    public void deletePais(Integer id) {paisRepository.delete(id);

    }
}
