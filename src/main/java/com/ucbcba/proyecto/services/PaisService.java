package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Pais;

public interface PaisService {

    Iterable<Pais> listAllPaises();

    Pais getPaisById(Integer id);

    Pais savePais(Pais pais);

    void deletePais(Integer id);

}
