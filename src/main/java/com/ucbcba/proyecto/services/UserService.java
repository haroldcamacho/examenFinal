package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Restaurant;
import com.ucbcba.proyecto.entities.User;

/**
 * Created by amolina on 10/05/17.
 */
public interface UserService {
    void save(User user);

    User getUserById(Integer id);

    User findUserByEmail(String email);

    void saveUserEdited(User user);
    User findByUsername(String username);
}