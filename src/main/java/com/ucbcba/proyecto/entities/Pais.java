package com.ucbcba.proyecto.entities;


import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="pais")
public class Pais {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty(message = "Debe ingresar una ciudad")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "pais", cascade = CascadeType.ALL)
    private Set<City> cities;

    public Set<City> getCities() {
        return cities;
    }

    public void setCities(Set<City> cities) {
        this.cities = cities;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }


}
