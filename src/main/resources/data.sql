INSERT INTO role(name)
VALUES("ADMIN");

INSERT INTO role(name)
VALUES("PROPIETARIO");

INSERT INTO role(name)
VALUES("CLIENTE");

INSERT INTO city(name)
VALUES("Cbba");

INSERT INTO city(name)
VALUES("Beni");

INSERT INTO city(name)
VALUES("Pando");

INSERT INTO restaurant (direction, name, phone, latitude, longitude, city_id)
VALUES("America","Ellis","4455555", -17.371, -66.143, 1);

INSERT INTO product(amount, name, restaurant_id)
VALUES(49, "Alfredo", 1);

INSERT INTO product(amount, name, restaurant_id)
VALUES(70, "Margarita", 1);

INSERT INTO product(amount, name, restaurant_id)
VALUES(50, "Carnivora", 1);

INSERT INTO restaurant (direction, name, phone, latitude, longitude, city_id)
VALUES("Circunvalacion","Malcriadas","4447777", -17.368, -66.145, 2);

INSERT INTO product(amount, name, restaurant_id)
VALUES(68, "Jamon y queso", 2);

INSERT INTO product(amount, name, restaurant_id)
VALUES(70, "Hawaiana", 2);

INSERT INTO product(amount, name, restaurant_id)
VALUES(95, "BBQ", 2);

--CLIENTE

INSERT INTO user (email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, product_id, city_id)
VALUES ("pepito@gmail.com","Pepito", "Mamani", "Pacata Alta", 4481520, 70764525, "$2a$10$/7.3krfR3zTzSljYIShk6OwiR404rQ./KgIuTwPgnm1dDLNSpTepy", "123", 1, 1, 1);

INSERT INTO user_role(user_id, role_id)
VALUES(1,3);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Circunvalacion",-17.367, -66.147, 1, 1);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Borda",-17.367, -66.148, 1, 1);

INSERT INTO user (email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, product_id, city_id)
VALUES ("juanita@gmail.com","Juanita", "Choque", "Pacata Baja", 42156467, 7070015,"$2a$10$/7.3krfR3zTzSljYIShk6OwiR404rQ./KgIuTwPgnm1dDLNSpTepy", "123", 2, 4, 2);

INSERT INTO user_role(user_id, role_id)
VALUES(2,3);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Girasoles",-17.367, -66.149, 1, 2);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Uyuni",-17.378, -66.149, 1, 2);
--

--PROPIETARIOS
INSERT INTO user (email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, city_id)
VALUES ("pablo@gmail.com","Pablo", "Gonsalez", "Pacata Alta", 4481520, 70764525, "$2a$10$/7.3krfR3zTzSljYIShk6OwiR404rQ./KgIuTwPgnm1dDLNSpTepy", "123", 1, 1);

INSERT INTO user_role(user_id, role_id)
VALUES(3,2);

INSERT INTO user (email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, city_id)
VALUES ("anita@gmail.com","Flor", "Villaroel", "Pacata Baja", 42156467, 7070015, "$2a$10$/7.3krfR3zTzSljYIShk6OwiR404rQ./KgIuTwPgnm1dDLNSpTepy", "123", 2, 2);

INSERT INTO user_role(user_id, role_id)
VALUES(4,2);
--

--ADMINISTRADOR
INSERT INTO user (email, username, lastname, address, cellphone, phone, password, password_Confirm)
VALUES ("diego@gmail.com","Diego", "Salgueiro", "hipermaxi", 4481520, 70764525, "$2a$10$/7.3krfR3zTzSljYIShk6OwiR404rQ./KgIuTwPgnm1dDLNSpTepy", "123");

INSERT INTO user_role(user_id, role_id)
VALUES(5,1);

INSERT INTO pais (name)
VALUES ("Bolivia");

INSERT INTO pais (name)
VALUES ("Argentina");

--

